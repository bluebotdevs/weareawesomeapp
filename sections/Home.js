import React from 'react';
import { Button, View, Text, TextInput, StyleSheet} from 'react-native';

class HomeScreen extends React.Component {

    constructor(props){
      super(props);

      this.state = {
        username: '',
        password: '',
      };

    }

    onLogin() {

      const { username, password } = this.state;
      
      var url = 'http://anima-vet.com/get_data.php';
      
      fetch('https://anima-vet.com/get_data.php', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: username,
          password: password,
        }),
      }).then((response) => response.json())
          .then((responseJson) => {
            
            let response = parseInt(responseJson.response);

            if(response){

              console.log("shouldve been logged in");
              this.props.navigation.navigate('About');
            }
            console.log(responseJson);

          })
          .catch((error) => {
            console.error(error);
          });

    }


    render() {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Welcome to WeAreAwesome App.</Text>
          <Text>-----</Text>
          <Text>Please log in to continue</Text>
          <TextInput
          value={this.state.username}
          onChangeText={(username) => this.setState({ username })}
          placeholder={'Username'}
          style={styles.input}
        />
        <TextInput
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          placeholder={'Password'}
          secureTextEntry={true}
          style={styles.input}
        />
        
        <Button
          title={'Login'}
          style={styles.input}
          onPress={this.onLogin.bind(this)}
        />
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ecf0f1',
    },
    input: {
      width: 200,
      height: 44,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 10,
    },
  });

  export default HomeScreen