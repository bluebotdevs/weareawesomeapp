import React from 'react';
import { Button, View, Text } from 'react-native';

class AboutScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
          <Text>THis is ABout us</Text>
          <Button
            title="Go back home if  you like"
            onPress={() => this.props.navigation.navigate('Home')}
          />
        </View>
      );
    }
  }

  export default AboutScreen