import React from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './sections/Home';
import AboutScreen from './sections/About';

const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen
  },
  About: {

    screen: AboutScreen
  }
});

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

